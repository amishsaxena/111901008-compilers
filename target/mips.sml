structure MIPS = struct

(* The registers of the mips machine *)
datatype reg =  zero 
                | at 
                | v0 
                | v1 
                | a0 
                | a1 
                | a2 
                | a3 
                | t0 
                | t1 
                | t2 
                | t3 
                | t4 
                | t5 
                | t6 
                | t7 
                | s0 
                | s1 
                | s2 
                | s3 
                | s4 
                | s5 
                | s6 
                | s7 
                | t8 
                | t9 
                | k0 
                | k1 
                | gp 
                | sp 
                | fp 
                | ra 
                |Imm of int

(* The instructions *)
datatype ('l, 't) inst =  Abs  	of 't * 't
                        | Add  	of 't * 't * 't
                        | Addi 	of 't * 't * 't
                        | Addu  of 't * 't * 't
                        | Addiu of 't * 't * 't
                        | And  	of 't * 't * 't
                        | Andi  of 't * 't * 't
                        | Div  	of 't * 't
                        | Divu  of 't * 't
                        | Div2  of 't * 't * 't
                        | Divu2 of 't * 't * 't
                        | Mul  	of 't * 't * 't
                        | Mulo  of 't * 't * 't
                        | Mulou of 't * 't * 't
                        | Mult  of 't * 't
                        | Multu of 't * 't
                        | Neg  	of 't * 't
                        | Negu  of 't * 't
                        | Nor  	of 't * 't * 't
                        | Not  	of 't * 't
                        | Or  	of 't * 't * 't
                        | Ori 	of 't * 't * 't
                        | Rem  	of 't * 't * 't
                        | Remu  of 't * 't * 't
                        | Rol  	of 't * 't * 't
                        | Ror  	of 't * 't * 't
                        | Sll  	of 't * 't * 't
                        | Sllv  of 't * 't * 't
                        | Sra 	of 't * 't * 't
                        | Srav  of 't * 't * 't
                        | Srl  	of 't * 't * 't
                        | Srlv  of 't * 't * 't
                        | Sub  	of 't * 't * 't
                        | Subu  of 't * 't * 't
                        | Xor  	of 't * 't * 't
                        | Xori 	of 't * 't * 't                     
                        | Li 	of 't * 't                          
                        | Lui 	of 't * 't                          
                        | Seq 	of 't * 't * 't                     
                        | Sge 	of 't * 't * 't
                        | Sgeu 	of 't * 't * 't
                        | Sgt 	of 't * 't * 't
                        | Sgtu 	of 't * 't * 't
                        | Sle 	of 't * 't * 't
                        | Sleu 	of 't * 't * 't
                        | Slt 	of 't * 't * 't
                        | Slti 	of 't * 't * 't
                        | Sltu 	of 't * 't * 't
                        | Sltiu of 't * 't * 't
                        | Sne 	of 't * 't * 't                     
                        | B 	of 'l                               
                        | Bczt 	of 'l
                        | Bczf 	of 'l
                        | Beq 	of 't * 't * 'l
                        | Beqz 	of 't * 'l
                        | Bge  	of 't * 't * 'l
                        | Bgeu 	of 't * 't * 'l 
                        | Bgez 	of 't * 'l
                        | Bgezal of 't * 'l
                        | Bgt 	of 't * 't * 'l
                        | Bgtu 	of 't * 't * 'l
                        | Bgtz 	of 't * 'l
                        | Ble 	of 't * 't * 'l
                        | Bleu 	of 't * 't * 'l
                        | Blez 	of 't * 'l
                        | Bltzal of 't * 'l
                        | Blt 	of 't * 't * 'l
                        | Bltu 	of 't * 't * 'l
                        | Bltz 	of 't * 'l
                        | Bne 	of 't * 't * 'l
                        | Bnez 	of 't * 'l
                        | J 	of 'l
                        | Jal 	of 'l
                        | Jalr 	of 't
                        | Jr 	of 't                               
                        | La 	of 't * 'l
                        | Lb 	of 't * 'l
                        | Lbu 	of 't * 'l
                        | Ld 	of 't * 'l
                        | Lh 	of 't * 'l
                        | Lhu 	of 't * 'l
                        | Lw 	of 't * 'l
                        | Lwcz 	of 't * 'l
                        | Lwl 	of 't * 'l
                        | Lwr 	of 't * 'l                          
                        | Sb 	of 't * 'l
                        | Sd 	of 't * 'l
                        | Sh 	of 't * 'l
                        | Sw 	of 't * 'l
                        | Swcz 	of 't * 'l
                        | Swl 	of 't * 'l
                        | Swr 	of 't * 'l
                        | Ulh 	of 't * 'l
                        | Ulhu 	of 't * 'l
                        | Ulw 	of 't * 'l
                        | Ush 	of 't * 'l
                        | Usw 	of 't * 'l                          
                        | Rfe                                       
                        | Syscall
                        | Break of int
                        | Nop                                       
                        | Move of 't * 't
                        | Mfhi of 't                                
                        | Mflo of 't
                        | Mthi of 't
                        | Mtlo of 't
                        | Mfcz of 't * 't
                        | Mtcz of 't * 't

datatype dir  = align of int
                | ascii of string
                | asciiz of string
                | byte of int list
                | data of string
                | extern of string*int
                | globl of string
                | half of int list
                | kdata of string
                | ktext of string
                | space of int
                | text of string
                | word of int list
            
datatype Label = User_Defined of string
               | TempLabel   of int

datatype ('l,'t) stmt =   Inst of ('l, 't) inst
                        | Dir of dir
                        | label of Label

fun   printReg zero = "$zero"
    | printReg at   = "$at"
    | printReg v0   = "$v0"
    | printReg v1   = "$v1"
    | printReg a0   = "$a0"
    | printReg a1   = "$a1"
    | printReg a2   = "$a2"
    | printReg a3   = "$a3"
    | printReg t0   = "$t0"
    | printReg t1   = "$t1"
    | printReg t2   = "$t2"
    | printReg t3   = "$t3"
    | printReg t4   = "$t4"
    | printReg t5   = "$t5"
    | printReg t6   = "$t6"
    | printReg t7   = "$t7"
    | printReg s0   = "$s0"
    | printReg s1   = "$s1"
    | printReg s2   = "$s2"
    | printReg s3   = "$s3"
    | printReg s4   = "$s4"
    | printReg s5   = "$s5"
    | printReg s6   = "$s6"
    | printReg s7   = "$s7"
    | printReg t8   = "$t8"
    | printReg t9   = "$t9"
    | printReg k0   = "$k0"
    | printReg k1   = "$k1"
    | printReg gp   = "$gp"
    | printReg sp   = "$sp"
    | printReg fp   = "$fp"
    | printReg ra   = "$ra"
    | printReg (Imm (i)) = Int.toString(i)

fun   printLabel (User_Defined s) = s
    | printLabel (TempLabel i)  = "$L"^Int.toString i


fun     printInst (Abs(r1,r2))              = "abs "^printReg(r1)^", "^printReg(r2)
    |   printInst (Add(r1,r2,r3))           = "add "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Addi(r1,r2,r3))          = "addi "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Addu(r1,r2,r3))          = "addu "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Addiu(r1,r2,r3))         = "addiu "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (And(r1,r2,r3))           = "and "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Andi(r1,r2,r3))          = "andi "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Div(r1,r2))              = "div "^printReg(r1)^", "^printReg(r2)
    |   printInst (Divu(r1,r2))             = "divu "^printReg(r1)^", "^printReg(r2)
    |   printInst (Div2(r1,r2,r3))          = "div "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Divu2(r1,r2,r3))         = "divu "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Mul(r1,r2,r3))           = "mul "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Mulo(r1,r2,r3))          = "mulo "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Mulou(r1,r2,r3))         = "mulou "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Mult(r1,r2))             = "mult "^printReg(r1)^", "^printReg(r2)
    |   printInst (Multu(r1,r2))            = "multu "^printReg(r1)^", "^printReg(r2)

    |   printInst (Neg(r1,r2))              = "neg "^printReg(r1)^", "^printReg(r2)
    |   printInst (Negu(r1,r2))             = "negu "^printReg(r1)^", "^printReg(r2)
    |   printInst (Nor(r1,r2,r3))           = "nor "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Not(r1,r2))              = "not "^printReg(r1)^", "^printReg(r2)
    |   printInst (Or(r1,r2,r3))            = "or "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Ori(r1,r2,r3))           = "ori "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Rem(r1,r2,r3))           = "rem "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Remu(r1,r2,r3))          = "remu "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Rol(r1,r2,r3))           = "rol "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Ror(r1,r2,r3))           = "ror "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)

    |   printInst (Sll(r1,r2,r3))           = "sll "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Sllv(r1,r2,r3))          = "sllv "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Sra(r1,r2,r3))           = "sra "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Srav(r1,r2,r3))          = "srav "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Srl(r1,r2,r3))           = "srl "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Srlv(r1,r2,r3))          = "srlv "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)

    |   printInst (Sub(r1,r2,r3))           = "sub "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Subu(r1,r2,r3))          = "subu "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)

    |   printInst (Xor(r1,r2,r3))           = "xor "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)
    |   printInst (Xori(r1,r2,r3))          = "xori "^printReg(r1)^", "^printReg(r2)^", "^printReg(r3)

    |   printInst (Li(r1,r2))               = "li "^printReg(r1)^", "^printReg(r2)                                        
    |   printInst (Lui(r1,r2))              = "lui "^printReg(r1)^", "^printReg(r2) 
    |   printInst (Seq(r1, r2, r3)) 	    = "seq "  ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)      
    |   printInst (Sge(r1, r2, r3))		    = "sge "  ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sgeu(r1, r2, r3))	    = "sgeu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sgt(r1, r2, r3))		    = "sgt "  ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sgtu(r1, r2, r3)) 		= "sgtu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sle(r1, r2, r3)) 		= "sle "  ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sleu(r1, r2, r3)) 		= "sleu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Slt(r1, r2, r3)) 		= "slt "  ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Slti(r1, r2, r3)) 		= "slti " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sltu(r1, r2, r3)) 		= "sltu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sltiu(r1, r2, r3)) 	    = "sltui " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    |   printInst (Sne(r1, r2, r3)) 		= "sne " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printReg(r3)
    
    |   printInst (B(l1)) 				    = "b " ^ printLabel(l1)
    |   printInst (Bczt(l1)) 				= "bczt " ^ printLabel(l1)
    |   printInst (Bczf(l1)) 				= "bczf " ^ printLabel(l1)
    |   printInst (Beq(r1, r2, l1)) 		= "beq " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Beqz(r1, l1)) 			= "beqz " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Bge(r1, r2, l1)) 		= "bge " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bgeu(r1, r2, l1)) 	    = "bgeu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bgez(r1, l1)) 			= "bgez " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Bgezal(r1, l1)) 		    = "bgezal " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Bgt(r1, r2, l1)) 		= "bgt " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bgtu(r1, r2, l1)) 		= "bgtu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bgtz(r1, l1)) 			= "bgtz " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ble(r1, r2, l1)) 		= "ble " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bleu(r1, r2, l1)) 	    = "bleu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Blez(r1, l1)) 			= "blez " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Bltzal(r1, l1)) 		    = "bltzal " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Blt(r1, r2, l1)) 		= "blt " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bltu(r1,r2, l1))         = "bltu " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bltz(r1, l1)) 			= "bltz " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Bne(r1, r2, l1)) 		= "bne " ^ printReg(r1) ^ ", " ^ printReg(r2) ^ ", " ^ printLabel(l1)
    |   printInst (Bnez(r1, l1)) 			= "bnez " ^ printReg(r1) ^ ", " ^ printLabel(l1)

    |   printInst (J(l1)) 				    = "j " ^ printLabel(l1)
    |   printInst (Jal(l1)) 			    = "jal " ^ printLabel(l1)
    |   printInst (Jalr(r1)) 			    = "jalr " ^ printReg(r1)
    |   printInst (Jr(r1)) 			        = "jr " ^ printReg(r1)

    |   printInst (La(r1, l1))		        = "la " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lb(r1, l1))		        = "lb " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lbu(r1, l1))		        = "lbu " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ld(r1, l1))		        = "ld " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lh(r1, l1))		        = "lh " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lhu(r1, l1))		        = "lhu " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lw(r1, l1))		        = "lw " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lwcz(r1, l1))		    = "lwcz " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lwl(r1, l1))		        = "lwl " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Lwr(r1, l1))		        = "lwr " ^ printReg(r1) ^ ", " ^ printLabel(l1)

    |   printInst (Sb(r1, l1))		        = "sb " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Sd(r1, l1))		        = "sd " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Sh(r1, l1))		        = "sh " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Sw(r1, l1))		        = "sw " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Swcz(r1, l1))		    = "swcz " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Swl(r1, l1))		        = "swl " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Swr(r1, l1))		        = "swr " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ulh(r1, l1))		        = "ulh " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ulhu(r1, l1))		    = "ulhu " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ulw(r1, l1))		        = "ulw " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Ush(r1, l1))		        = "ush " ^ printReg(r1) ^ ", " ^ printLabel(l1)
    |   printInst (Usw(r1, l1))		        = "usw " ^ printReg(r1) ^ ", " ^ printLabel(l1)  

    |   printInst (Rfe)                     = "rfe"
    |   printInst (Syscall)                 = "syscall"
    |   printInst (Break(i))                = "break "^Int.toString(i)
    |   printInst (Nop)                     = "nop"  
    |   printInst (Move(r1, r2))            = "move "^ printReg(r1)^", "^printReg(r2) 
    |   printInst (Mfhi(l1))                = "mfhi "^ printReg(l1)
    |   printInst (Mflo(l1))                = "mflo "^ printReg(l1)
    |   printInst (Mthi(l1))                = "mthi "^ printReg(l1)
    |   printInst (Mtlo(l1))                = "mtlo "^ printReg(l1)
    |   printInst (Mfcz(l1, l2))            = "mfcz "^ printReg(l1)^", "^printReg(l2)
    |   printInst (Mtcz(l1, l2))            = "mtcz "^ printReg(l1)^", "^printReg(l2)

fun printList []     = ""
  | printList (x::[]) = Int.toString x
  | printList (x::xs) = (Int.toString x) ^ ", "^(printList xs)

fun   printDir (align(n))   =   ".align " ^ Int.toString(n)
    | printDir (ascii(n))   =   ".ascii " ^ n
    | printDir (asciiz(n))  =   ".asciiz " ^ n
    | printDir (byte(n))    =   ".byte " ^ printList(n)
    | printDir (data(n))    =   ".data " ^ n
    | printDir (extern(s,n))=   ".extern " ^ s ^ " " ^ Int.toString(n)
    | printDir (globl(s))   =   ".globl " ^ s
    | printDir (half(x))    =   ".half " ^ printList(x)
    | printDir (kdata(s))   =   ".kdata " ^ s
    | printDir (ktext(s))   =   ".ktext " ^ s
    | printDir (space(n))   =   ".space " ^ Int.toString(n)
    | printDir (text(s))    =   ".text " ^ s
    | printDir (word(x))    =   ".word " ^ printList(x)

fun   printStmt (Inst(i))  = printInst i
    | printStmt (Dir(d)) = printDir d
    | printStmt (label(l)) = printLabel l ^ ":" 

fun printProg [] = ""
    |   printProg (x::xs) = (printStmt x) ^ "\n" ^ (printProg xs)
end
