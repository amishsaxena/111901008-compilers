# Instructions

- Use `make` to build the executable files. 
- The test cases are present in the `target/test` directory, namely as : `test1.inp` , `test2.inp` , `test3.inp`.
- `make test` can be used to run the executale generated on each of the test files.
- The corresponding outputs are generated in `target/test` directory itself with the names : `out1.mips` , `out2.mips` , `out3.mips`
- 
- In order to see the output of the a custom test on the executable with SPIM, do the follwoing : 
  - Run `make` in the `target` directory of the repository.
  - Use  `./ec test.inp > output.mips` to save the output in the *output.mips* file.
  - Spawn SPIM using `spim` in the termminal (in the same directory where the output.mips is generated). 
  - Type `load "output.mips"` in the spim terminal to load the file.
  - Type `run` to run the MIPS file and get the output
  - Use `Ctrl-D` to exit spim. 
  - Use the above steps again to try out any other test case.
  - *Note* : The out files generated from the already present test cases have to be tried in spim by using the appropriate directory.


