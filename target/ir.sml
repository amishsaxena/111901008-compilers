structure IR : sig
  type inst = (string, Temp.temp) MIPS.inst
  type stmt = (string, Temp.temp) MIPS.stmt
  type prog = stmt list
  val P_pr     : prog -> string
  val PPrStmt : stmt -> string
  val PPrInst : inst -> string
  
  end = struct

  fun PPrInst (MIPS.Add(a, b, c))   = Temp.temp_to_String a ^ " := " ^ Temp.temp_to_String b ^ " + " ^ Temp.temp_to_String c
    | PPrInst (MIPS.Mul(a, b, c))   = Temp.temp_to_String a ^ " := " ^ Temp.temp_to_String b ^ " * " ^ Temp.temp_to_String c
    | PPrInst (MIPS.Sub(a, b, c))   = Temp.temp_to_String a ^ " := " ^ Temp.temp_to_String b ^ " - " ^ Temp.temp_to_String c
    | PPrInst (MIPS.Div2(a, b, c))  = Temp.temp_to_String a ^ " := " ^ Temp.temp_to_String b ^ " / " ^ Temp.temp_to_String c

  fun PPrStmt (MIPS.Inst ex)  = PPrInst ex 

  fun P_pr []    = ""
  |   P_pr x::xs = PPrStmt(x) ^ "\n" ^ P_pr(xs)

end