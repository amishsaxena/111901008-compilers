(* The AST for the MIPS *)

structure Ast = struct
    datatype Expr  = Const of int
                    | Variable of string
                    | Op  of Expr * BinOp * Expr

     and BinOp = Plus
               | Mul
               | Minus
               | Divi

    datatype Stmt   = Assignment of string * Expr
                    | Print of Expr

                    | For of (string * int * int * Stmt list)
      
    datatype prog = EXPS of (Stmt list)

    (* Arithematic Operation functions *)
    fun plus  a b = Op (a, Plus, b)
    fun mul   a b = Op (a, Mul, b)
    fun minus a b = Op (a, Minus, b)
    fun divi  a b = Op (a, Divi, b)

    fun assign v e = Assignment(v,e)

    fun for (Variable(v)) (Const(a)) (Const(b)) en = For(v,a,b,en)
    |   for _ _ _ _                                = (Print(Variable("error")))
end