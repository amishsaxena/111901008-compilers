
(* type block = inst list
val basicBlocks : inst list -> block list *)


signature INST = sig
    type t   
    val is_Jump_like   : t -> bool
    val is_Target     : t -> bool
end

structure MIPSInst : INST = struct
    type t = (string, Temp.temp) MIPS.stmt

    fun is_Jump_like (MIPS.Inst (MIPS.J x)) = true 
      | is_Jump_like (MIPS.Inst (MIPS.Bgt x)) = true 
      | is_Jump_like _ = false
    
    fun is_Target (MIPS.label x) = true 
      | is_Target _ = false
      
end

functor BasicBlocks (I : INST) = struct

    structure Inst = I
    type block = I.t list
    
    val is_Jump_like  = I.is_Jump_like
    val is_Target     = I.is_Target
    
    val current_block = ref ([] : block) 

    fun basicBlocks ((x :: xs) : I.t list) = let
                                                  val temp_block = ref (!current_block)
                                              in
                                                  if (is_Jump_like x) then
                                                      (temp_block := [x] @ !temp_block;
                                                      current_block := [];
                                                      [temp_block] @ (basicBlocks xs))
                                                  else if (is_Target x) then
                                                      (current_block := [];
                                                      [temp_block] @ (basicBlocks xs))
                                                  else
                                                      (current_block := [x] @ !current_block;
                                                      basicBlocks xs)
                                              end
    | basicBlocks _ = [ref (!current_block)]
end

structure MIPSBasicBlocks = BasicBlocks (MIPSInst)
