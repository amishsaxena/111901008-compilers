structure Translate =
struct

fun temp_to_Reg 0 = MIPS.t0
  | temp_to_Reg 1 = MIPS.t1
  | temp_to_Reg 2 = MIPS.t2
  | temp_to_Reg 3 = MIPS.t3
  | temp_to_Reg 4 = MIPS.t4
  | temp_to_Reg 5 = MIPS.t5
  | temp_to_Reg 6 = MIPS.t6
  | temp_to_Reg 7 = MIPS.t7
  | temp_to_Reg _ = MIPS.Imm(1)

fun temp_to_Label n = MIPS.TempLabel(n)

fun compileExpr env t (Ast.Const x) = ([MIPS.Inst(MIPS.Li(temp_to_Reg(t),MIPS.Imm(x)))], env)

  | compileExpr env t (Ast.Variable v) = let
                                      val x = case AtomMap.find(env, Atom.atom(v)) of
                                              NONE =>  let
                                                          val newT = Temp.newtemp()
                                                          val new_env = AtomMap.insert(env,Atom.atom(v),newT)
                                                        in  
                                                          ([], new_env)
                                                        end
                                            | (SOME(u)) => ([MIPS.Inst(MIPS.Move(temp_to_Reg(t), temp_to_Reg(u)))], env)
                                          in
                                            x
                                          end

  | compileExpr env t (Ast.Op(e1, Ast.Plus, e2)) =  let 
                                                    val a = Temp.newtemp()
                                                    val b = Temp.newtemp()
                                                    val (result_1, env1) = compileExpr env a e1
                                                    val (result_2, env2) = compileExpr env1 b e2
                                                  in
                                                    (result_1 @ result_2 @ [MIPS.Inst(MIPS.Add(temp_to_Reg(t), temp_to_Reg(a), temp_to_Reg(b)))], env2)
                                                  end

  | compileExpr env t (Ast.Op(e1, Ast.Mul, e2)) =  let 
                                                    val a = Temp.newtemp()
                                                    val b = Temp.newtemp()
                                                    val (result_1, env1) = compileExpr env a e1
                                                    val (result_2, env2) = compileExpr env1 b e2
                                                  in
                                                    (result_1 @ result_2 @ [MIPS.Inst(MIPS.Mul(temp_to_Reg(t), temp_to_Reg(a), temp_to_Reg(b)))], env2)
                                                  end

  | compileExpr env t (Ast.Op(e1, Ast.Minus, e2)) =  let 
                                                    val a = Temp.newtemp()
                                                    val b = Temp.newtemp()
                                                    val (result_1, env1) = compileExpr env a e1
                                                    val (result_2, env2) = compileExpr env1 b e2
                                                  in
                                                    (result_1 @ result_2 @ [MIPS.Inst(MIPS.Sub(temp_to_Reg(t), temp_to_Reg(a), temp_to_Reg(b)))], env2)
                                                  end

  | compileExpr env t (Ast.Op(e1, Ast.Divi, e2)) =  let 
                                                    val a = Temp.newtemp()
                                                    val b = Temp.newtemp()
                                                    val (result_1, env1) = compileExpr env a e1
                                                    val (result_2, env2) = compileExpr env1 b e2
                                                  in
                                                    (result_1 @ result_2 @ [MIPS.Inst(MIPS.Div2(temp_to_Reg(t), temp_to_Reg(a), temp_to_Reg(b)))], env2)
                                                  end


fun compileStmt env (Ast.Assignment(x,exp)) =  let
                                            val value = case AtomMap.find(env, Atom.atom(x)) of
                                              (SOME(u)) => let
                                                              val (result_1, env1) = compileExpr env u exp
                                                            in
                                                              (result_1, env)
                                                            end
                                              |NONE =>  let
                                                          val newT = Temp.newtemp()
                                                          val new_env = AtomMap.insert(env,Atom.atom(x),newT)
                                                          val (result_1, env2) = compileExpr new_env newT exp
                                                        in  
                                                          (result_1, env2)
                                                        end
                                            in
                                              value
                                            end

  | compileStmt env (Ast.Print (e)) = let 
                                        val t1 = Temp.newtemp()
                                        val (res,env1) =  compileExpr env t1 e
                                      in
                                        (
                                          res @ 
                                          [MIPS.Inst(MIPS.Move(MIPS.a0,temp_to_Reg(t1)))] @ 
                                          [MIPS.Inst(MIPS.Li(MIPS.v0,MIPS.Imm(1)))] @ 
                                          [MIPS.Inst(MIPS.Syscall)] @ 
                                          [MIPS.Inst(MIPS.Li(MIPS.a0,MIPS.Imm(10)))] @ 
                                          [MIPS.Inst(MIPS.Li(MIPS.v0,MIPS.Imm(11)))] @ 
                                          [MIPS.Inst(MIPS.Syscall)] 
                                          , env1)
                                      end

  | compileStmt env (Ast.For(v,c1,c2,stmtList)) =  let
                                                      val temp_loop = Temp.newtemp()
                                                      val env_loop = AtomMap.insert(env,Atom.atom(v),temp_loop)
                                                      val loop_Label = Temp.newLabel()
                                                      val loop_stop_temp = Temp.newtemp()
                                                      val loop_exit_Label = Temp.newLabel()
                                                      fun compileLoop e [] = []
                                                        | compileLoop e (x::xs) =   let
                                                                                        val (res,env1) = compileStmt e x
                                                                                      in
                                                                                        res @ compileLoop env1 xs
                                                                                    end
                                                      val loop_stmts = compileLoop env_loop stmtList
                                                    in
                                                      ([
                                                        MIPS.Inst(MIPS.Li(temp_to_Reg(temp_loop),MIPS.Imm(c1))), 
                                                        MIPS.Inst(MIPS.Li(temp_to_Reg(loop_stop_temp),MIPS.Imm(c2))),
                                                        MIPS.label(temp_to_Label(loop_Label)),
                                                        MIPS.Inst(MIPS.Bgt(temp_to_Reg(temp_loop), temp_to_Reg(loop_stop_temp), temp_to_Label(loop_exit_Label)))
                                                       ]
                                                       @ loop_stmts @ 
                                                       [
                                                        MIPS.Inst(MIPS.Addi(temp_to_Reg(temp_loop),temp_to_Reg(temp_loop),MIPS.Imm(1))),
                                                        MIPS.Inst(MIPS.J(temp_to_Label(loop_Label))),
                                                        MIPS.label(temp_to_Label(loop_exit_Label))
                                                       ], 
                                                      env)
                                                    end


fun compileProg env [] = [MIPS.Inst(MIPS.Li(MIPS.v0,MIPS.Imm(10)))] @ [MIPS.Inst(MIPS.Syscall)]

  | compileProg env (x::xs) = let
                              val (res,next_env) = compileStmt env x
                            in
                              res @ compileProg next_env xs
                            end
                
fun compile program = [MIPS.Dir(MIPS.globl("main")), MIPS.label(MIPS.User_Defined("main"))] @ compileProg AtomMap.empty program

end

