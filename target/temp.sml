signature TEMP =
  sig
    type temp = int
    type label = int
    val newLabel   : unit -> label
    val newtemp    : unit -> temp
    val temp_to_String : temp -> string
    val label_to_String : label -> string
  end

structure Temp :> TEMP = struct

  type label = int
  type temp  = int 

  val nextLabel      = ref 0
  val nextTemp       = ref 0 
  fun newtemp  _     = let 
                            val t = !nextTemp 
                        in 
                            if (t>8) 
                                then (TextIO.output(TextIO.stdErr, "Error: Available Temp registers exhausted \n"); OS.Process.exit OS.Process.failure)
                            else 
                                nextTemp := t+1; t 
                        end

  fun label_to_String l = "l"^Int.toString l

  fun temp_to_String t = "t" ^ Int.toString t

  fun newLabel _ = let 
                    val l = !nextLabel 
                  in 
                    nextLabel := l+1; l
                  end



end