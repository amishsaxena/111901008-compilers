# Lab 0 
Lab pre-requisistes finished.

---------------

# Lab 1
The following files were updated : 
```    
    modified:   Makefile
    modified:   ast.sml
    modified:   expr.grm
    modified:   expr.lex
    modified:   rp.lex
    modified:   test.expr
    modified:   test.inp
```
-   modified:   **Makefile**
    -   Commented out the running of `test.inp` in line `75`
-   modified:   **ast.sml**
    -   Add function definitions for `Div` analogous to `Mul` in `binOpDenote` and `binOpDenote`
    -   Define the function `div`
-   modified:   **expr.grm**
    -   Add terminals for DIV , Left Bracket and Right Bracket
    -   Add grammar for the bracket usage
-    modified:   **expr.lex**
    -   Add lexicography corresponding to DIV , Left Bracket and Right Bracket
-   modified:   **rp.lex**
    -   Added "/" analogoues to "*"
-   modified:   **test.expr**
    -   Added some test cases for the expressions
-   modified:   **CHANGELOG.md**
    -   Changes added for Lab1

### Tests Passed Correctly with the output : 
```
❯ make test 
/home/amish/IIT/SEM6/Compilers/111901008-compilers/src/ec test.expr | /home/amish/IIT/SEM6/Compilers/111901008-compilers/src/rp
42
56
15
3
3
0
50
152
~10
0
```
---------------


# Lab 2

Done making mips.sml 

# Lab 3

-   The following files are updated wrt similarities with Reverse Polish Compiler
    -   expr.lex
    -   expr.grm
    -   translate.sml
    -   ast.sml
    -   ec.sml
    -   ec.mlb
    -   Makefile
-   The compiler now catpures a calculator like syntax with variables
-   