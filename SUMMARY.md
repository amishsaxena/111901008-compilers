# Summary

### 1. Update Reverse Polish Compiler

The following Changes(or additions) were done to the given code of Reverse Polish Compiler in the `reverse-polish` directory : 

- Add division arithematic operation support 
- Add support for the Brackets (parenthesis)
- Define an precedence order of arithematic operations
- Changes were made in the ast and parsing files (expr.grm, expr.lex) to perform the above.

### 2. MIPS structure to contain definition of MIPS's AST

The following Changes(or additions) were done in the `target` directory: 

- In the `mips.sml` file, made a structure MIPS to capture : 
  - The registers of MIPS
  - Data-types for Instructions, Directives and Labels in the MIPS
  - Data-type to capture the statements in MIPS
- Functions were defined to perfrom the pretty printing for program which depended upon: 
  -  Pretty printing of the statements (which was done using)
  -  Pretty printing of Registers, Instructions, Directives and Labels
  -  (which were defined earlier)

### 3. Language of Variables  

This is where the basic part of the compiler starts taking shape. 
We use some of the files from the reverse-polish as an analogy to build our compiler. 

We expand beyond the realms of the reverse-polish to make a compiler that can : 
-   Use text based variables 
-   Assign values to those variables
-   Perform arithematic operations on those variables as well using other integers

We generate a MIPS code as an output that can be loaded into the SPIM to run. 

The following Changes(or additions) were done in the `target` directory: 

-   `ast.sml` is used to define an Abstract Syntax Tree for the compiler. We capture the statements, expressions and assign types in the structure.
-   `expr.grm` and `expr.lex` are modified to accomodate the variables , assign operator and suitable changes are made to the grammar. 
-   This enabled the parsing oart of the compiler.
-   `translate.sml` is used to generate the MIPS assembly code for the output. 
-   We use different compile functions to systematically proccess the parsed input and compose the final assemply code in MIPS assembly language.
-   Additionally, change to files like `Makefile`, `ec.mlb`, `ec.sml`, etc were made to account for the proper compilation of the compiler using mlton.
- Test cases are made and put in the `tests` directory to test the functioning of our compiler. 


### 4. For Loops

We further biuld upon the previous part to add the support for simple iterative for-loop in the compiler. 

- We make changes to support the generation of new temp labels for the loop.
- We also add the particulars to the ast for the FOR loop and correspondingly modify the parsing files too. 
- Additionally, we add to the function compileStmt to process the FOR loop code, take care of the respective environments for the local variables and the global ones as well as assemble the MIPS code for the same.

### Additon of newline 

This is an *additional feature* implemented to add a newline at the end of each of the print statement. This was done to imporve the readability after the generated MIPS code is run in the SPIM. 

- This was implemented simple by adding some instructions to add a newline character and a syscall to print it after every print expression . 

### 6. Basic Blocks

- The basics blocks were implemented by completing the given code.
- MIPSInst structure was defined
- The basicBlocks functor was completed.
- This is used to identify the blocks from a list of instructions. 

### Instructions to RUN/Test the compiler : 

- Use `make` to build the executable files. 
- The test cases are present in the `target/test` directory, namely as : `test1.inp` , `test2.inp` , `test3.inp`.
- `make test` can be used to run the executale generated on each of the test files.
- The corresponding outputs are generated in `target/test` directory itself with the names : `out1.mips` , `out2.mips` , `out3.mips`
- 
- In order to see the output of the a custom test on the executable with SPIM, do the follwoing : 
  - Run `make` in the `target` directory of the repository.
  - Use  `./ec test.inp > output.mips` to save the output in the *output.mips* file.
  - Spawn SPIM using `spim` in the termminal (in the same directory where the output.mips is generated). 
  - Type `load "output.mips"` in the spim terminal to load the file.
  - Type `run` to run the MIPS file and get the output
  - Use `Ctrl-D` to exit spim. 
  - Use the above steps again to try out any other test case.
  - *Note* : The out files generated from the already present test cases have to be tried in spim by using the appropriate directory.


